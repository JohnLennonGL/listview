package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {


    private ListView Lista;
    private String[] Itens = {"Parelheiros","Vargem grande","Colonia","novo america","santa terezinha","barragem","silveira","marsilac","","embu guarçu","asassrrtersed","gtgrfdggfds","fsdsfrgfdfgfd","fdhgdgfgf","sasasa","ewewewew","asasasaa","asassrrtersed","gtgrfdggfds","fsdsfrgfdfgfd","fdhgdgfgf"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Lista = findViewById(R.id.ListViewID);


        //criar adaptador, ele tem 4 parametros
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                Itens
        );
        Lista.setAdapter(adaptador);

        /// lista ja esta pronta, ja da pra vizualizar


        // criar evento de click na lista
        Lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int codigoPosicao = position;

                String valorClicado = Lista.getItemAtPosition(codigoPosicao).toString();
                Toast.makeText(getApplicationContext(),valorClicado, Toast.LENGTH_LONG).show();
            }
        });

    }
}
